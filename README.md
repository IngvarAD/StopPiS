# Protester Companion App
End of October 2020, Poland, Warsaw. The Constitutional Tribunal rules that abortion is illegal in the event of serious and irreversible abnormalities in the fetus. This is the second wave of the COVID pandemic, but people are angry and taking to the streets... It starts as some story, but these are the real events that gave me the idea and motivation to create this app. It's second android app that I'm making and first that I'm writing from scratch.

[[_TOC_]]

## What this app is
It aims to help people protest in safe manner:
 - You can call for help easy, just with touch of a button.
 - If you see someone being arrested, you can make a call to legal aid with one click
 - Has a guide on what to do during a protest, what the police can do, and what to do in a situation of arrest (now just in Polish)

## Why It's named "StopPiS"
In Poland, the currently ruling party is law and justice ([Prawo i Sprawiedliwość](https://en.wikipedia.org/wiki/Law_and_Justice), PiS for short), national conservative and right-wing populist political party. They are against any abortion, contraception, LGBT+... and want to divide Poland. So the title of the application alludes to the current movement against this party. ***** ***

## TODO
1. [x] Send distress SMS with location
   1. [ ] Make message content customisable
1. [ ] Improve getting location
1. [x] Call legal help number (only for Warsow now)
   1. [ ] Option to add numbers for other Cities/Countries
   1. [ ] Select City/Country where you are protesting
1. [x] Legal informations
1. [ ] Add translations
   1. [x] Polish
   1. [ ] English (Partially)
1. [ ] Integration with external IM
   1. [ ] Signal
   1. [ ] Telegram
1. [ ] Map
1. [ ] Wireless mesh network
   1. [ ] Local IM
   1. [ ] Share map points