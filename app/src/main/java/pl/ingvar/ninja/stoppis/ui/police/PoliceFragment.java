package pl.ingvar.ninja.stoppis.ui.police;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import pl.ingvar.ninja.stoppis.R;

public class PoliceFragment extends Fragment {

    private PoliceViewModel mViewModel;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<String[]> QUESTIONS = new ArrayList<>();

    public static PoliceFragment newInstance() {
        return new PoliceFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_police, container, false);

        readQuestionsData();
        recyclerView = root.findViewById(R.id.QAList);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ManualAdapter(QUESTIONS);
        recyclerView.setAdapter(mAdapter);

        return root;
    }

    private void readQuestionsData() {
        InputStream is = getResources().openRawResource(R.raw.qa);
        BufferedReader bReader = new BufferedReader(
                new InputStreamReader(is, StandardCharsets.UTF_8)
        );
        String line = "";
        try {
            while ((line = bReader.readLine()) != null) {
                String[] row = line.split(";");
                QUESTIONS.add(row);
            }
        } catch (IOException e) {
            Log.wtf("PoliceFragment", "Error reading on line" + line, e);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PoliceViewModel.class);
    }

}