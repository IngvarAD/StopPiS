package pl.ingvar.ninja.stoppis.ui.info;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.PriorityQueue;

import pl.ingvar.ninja.stoppis.PhoneSample;
import pl.ingvar.ninja.stoppis.R;

public class PhonesRecyclerViewAdapter  extends RecyclerView.Adapter<PhonesRecyclerViewAdapter.PhonesViewHolder> {
    private List<PhoneSample> PHONES;

    private Context context;

    public static class PhonesViewHolder extends RecyclerView.ViewHolder {
        public TextView nameView;
        public TextView numberView;
        public ImageButton callButton;

        public PhonesViewHolder(View v) {
            super(v);
            nameView = v.findViewById(R.id.name_placeholder);
            numberView = v.findViewById(R.id.number_placeholder);
            callButton = v.findViewById(R.id.call_me_maybe);
        }
    }

    public PhonesRecyclerViewAdapter( List<PhoneSample> data) {
        this.PHONES = data;
    }

    @Override
    public PhonesRecyclerViewAdapter.PhonesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.number_item, parent, false);

        context = view.getContext();

        return new PhonesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhonesViewHolder holder, int position) {
        final PhoneSample row = PHONES.get(position);
        holder.nameView.setText(row.getName());
        holder.numberView.setText(row.getNumber());

        holder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call_number = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+row.getNumber()));
                v.getContext().startActivity(call_number);
                Log.d("PhoncesRecyclerView", "Calling number: " + row.getNumber());
            }
        });
    }

    @Override
    public int getItemCount() {
        return PHONES.size();
    }
}
