package pl.ingvar.ninja.stoppis.ui.info;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InfoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public InfoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Zadźwoń po pomoc!");
    }

    public LiveData<String> getText() {
        return mText;
    }


}