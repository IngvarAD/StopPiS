package pl.ingvar.ninja.stoppis.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import pl.ingvar.ninja.stoppis.EmergencyActions;
import pl.ingvar.ninja.stoppis.MainActivity;
import pl.ingvar.ninja.stoppis.R;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private HomeViewModel homeViewModel;
    private EmergencyActions emergencyActions;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor prefEditor;

    private class PrefWatcher implements TextWatcher {
        private View view;

        private PrefWatcher(View v) {
            this.view = v;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //Don't need to do anything here
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //Don't need to do anything here
        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            switch (view.getId()){
                case R.id.nameText:
                    saveUserData("UserName",text);
                    break;
                case R.id.surnameText:
                    saveUserData("UserSurname", text);
                    break;
                case R.id.contactPhoneText:
                    saveUserData("UserContactPhone", text);
                    break;
            }

            updateSMSText(view.getRootView());
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        prefEditor = sharedPref.edit();

        emergencyActions = new EmergencyActions(getActivity());

        loadSavedDataToUi(root);

        Button callMedic = root.findViewById(R.id.callMedicsBtn);
        callMedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMedicPopup();
            }
        });

        EditText name_field = root.findViewById(R.id.nameText);
        name_field.addTextChangedListener(new PrefWatcher(name_field));
        EditText surname_field = root.findViewById(R.id.surnameText);
        surname_field.addTextChangedListener(new PrefWatcher(surname_field));
        EditText contact_phone_text = root.findViewById(R.id.contactPhoneText);
        contact_phone_text.addTextChangedListener(new PrefWatcher(contact_phone_text));

        updateLocation();
        updateSMSText(root);

        return root;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.medic_help_sms_btn:
                sendMedicMessage("sms");
                break;
            case R.id.medic_help_telegram_btn:
                sendMedicMessage("telegram");
                break;
            case R.id.medic_help_signal_btn:
                sendMedicMessage("signal");
                break;
            case R.id.callMedicsBtn:
                sendEmergencySMS();
                break;
        }
        v.setEnabled(false);
        final View btn = v;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btn.setEnabled(true);
            }
        }, 30000); //Block clicked button for 30 seconds
    }

    private void updateSMSText(View v) {
        String user = sharedPref.getString("UserName","Anonimowy") + " " +
                sharedPref.getString("UserSurname", "Anonim");
        String msg = "Policja próbuje mnie zatrzymać! Moja lokalizacja " +
                sharedPref.getString("MapLinks","null") +
                " Jak wkrótcę nie odpowiem powiadom grupę antyrepresyjną (+48 722-196-139)! " +
                user;
        ((TextView) v.findViewById(R.id.smsPlaceholder)).setText(msg);
    }

    private void loadSavedDataToUi(View v) {
        EditText name_field = v.findViewById(R.id.nameText);
        EditText surname_field = v.findViewById(R.id.surnameText);
        EditText contact_phone_text = v.findViewById(R.id.contactPhoneText);
        name_field.setText(sharedPref.getString("UserName",""));
        surname_field.setText(sharedPref.getString("UserSurname",""));
        contact_phone_text.setText(sharedPref.getString("UserContactPhone","+48"));
    }

    private void saveUserData(String key, String val) {
        prefEditor.putString(key, val);
        prefEditor.apply();
    }

    private void sendMedicMessage(String type) {
        emergencyActions.sendMessageWithLocation(type);
    }

    private void sendEmergencySMS() {
        emergencyActions.sendContactEmergencyMessage();
    }

    private void showMedicPopup(){
        updateLocation();
        Dialog medicDialogue = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        medicDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(100,0,0,0)));
        medicDialogue.setContentView(R.layout.medic_popup);
        medicDialogue.setCancelable(true);
        medicDialogue.setCanceledOnTouchOutside(true);
        medicDialogue.show();

        Button sms = medicDialogue.findViewById(R.id.medic_help_sms_btn);
        sms.setOnClickListener(this);
        Button telegram = medicDialogue.findViewById(R.id.medic_help_telegram_btn);
        telegram.setOnClickListener(this);
        Button signal = medicDialogue.findViewById(R.id.medic_help_signal_btn);
        signal.setOnClickListener(this);
    }

    private void updateLocation() {
        ((MainActivity) getActivity()).getLastLocation();
    }
}