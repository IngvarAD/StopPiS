package pl.ingvar.ninja.stoppis;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance;
    private String[] cities = {"Warszawa"};
    private EmergencyActions emergencyActions;
    private Handler handler = new Handler();

    private FusedLocationProviderClient fusedLocationClient;
    private SharedPreferences PREFERENCES;
    private SharedPreferences.Editor PREF_EDITOR;

    private Boolean restartVolumePressCountRunning = false;
    private Runnable restartVolumePressCount = new Runnable() {
        @Override
        public void run() {
            emergencyActions.restartCount();
            restartVolumePressCountRunning = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar topBar = findViewById(R.id.top_action_bar);
        setSupportActionBar(topBar);
        showLogoInTopBar();

        instance = this;
        emergencyActions = new EmergencyActions(this);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        PREFERENCES = this.getPreferences(Context.MODE_PRIVATE);
        PREF_EDITOR = PREFERENCES.edit();
        getLastLocation();

        askForPermissions(Manifest.permission.SEND_SMS);
        askForPermissions(Manifest.permission.ACCESS_COARSE_LOCATION);
        askForPermissions(Manifest.permission.ACCESS_FINE_LOCATION);
        askForPermissions(Manifest.permission.CALL_PHONE);
    }

    private void askForPermissions(String permission) {
        Context con = getApplicationContext();
        if (ContextCompat.checkSelfPermission(
                con, permission) ==
                PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[] { permission },1);
        }
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public FusedLocationProviderClient getLocationClient() {
        return fusedLocationClient;
    }

    public void getLastLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null){
                                emergencyActions.createMapLinks(location);
                        }
                    }
            });
        }
    }

    private void showLogoInTopBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.stoppis_launcher_foreground);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Switch is here for future to add options button
        switch (item.getItemId()) {
            case R.id.select_city:
                    Toast.makeText(getApplicationContext(),"StopPiS v0.01\nMade by ingvar.ninja", Toast.LENGTH_LONG).show();
                    break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    //Detect 5 times pressed volume down
    //TODO Make button configurable
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
            emergencyActions.volumeDownPressed();
            if (!restartVolumePressCountRunning) {
                restartVolumePressCountRunning = true;
                handler.postDelayed(restartVolumePressCount, 5000); // 5 seconds
            }
        }
        return true;
    }

    public String[] getCities(){
        return cities;
    }
}