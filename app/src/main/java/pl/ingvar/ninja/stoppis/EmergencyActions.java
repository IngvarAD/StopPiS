package pl.ingvar.ninja.stoppis;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.HashMap;

public class EmergencyActions {

    private Activity CONTEXT;
    private final Integer KEY_COUNT = 5;
    private Integer EMERGENCY_COUNT = 0;
    private SharedPreferences PREFERENCES;
    private FusedLocationProviderClient locationClient;

    private String MAP_LINKS;

    private static HashMap<String, String> EMERGENCY_NUMBER = new HashMap<>();

    public EmergencyActions(Activity context) {
        EMERGENCY_NUMBER.put("default","0");
//        EMERGENCY_NUMBER.put("default","112");
//        EMERGENCY_NUMBER.put("Warszawa","550835872");

        CONTEXT = context;
        PREFERENCES = CONTEXT.getPreferences(Context.MODE_PRIVATE);
        locationClient = MainActivity.getInstance().getLocationClient();
    }

    public void sendMessageWithLocation(final String msg_type) {
        if (ContextCompat.checkSelfPermission(CONTEXT, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(CONTEXT, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            locationClient.getLastLocation()
                    .addOnSuccessListener(CONTEXT, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null){
                                createMapLinks(location);
                                medicMessageSender(msg_type);
                            }
                        }
                    });
        }
    }

    public void createMapLinks(Location coordinates) {
        String mlat = String.valueOf(coordinates.getLatitude());
        String mlong = String.valueOf(coordinates.getLongitude());
        String google_maps_link = "https://google.com/maps/search/?api=1&query=" + mlat + ", " + mlong;
        String osm_link = "https://openstreetmap.org/?mlat" + mlat + "&mlon=" + mlong +
                "#map=18/" + mlat + "/" + mlong;

        MAP_LINKS = google_maps_link + "\n" + osm_link;
        PREFERENCES.edit().putString("MapLinks", MAP_LINKS).apply();

        Log.d("Emergency Actions", "Map links created " + MAP_LINKS);
    }

    public String getMAP_LINKS() {
        return MAP_LINKS;
    }

    public void volumeDownPressed(){ volumeDownPressedAction(); }

    public void restartCount(){
        EMERGENCY_COUNT = 0;
    }

    public void medicMessageSender(String msg_type) {
        String sms_message = "This will be distress message with all necessary data";

        //TODO add option for sending medical emergency messages
        switch (msg_type){
            case "sms":
                String number = EMERGENCY_NUMBER.get(PREFERENCES.getString("City","default"));
                sendSMS(sms_message, number);
                break;
            case "signal":
                Toast.makeText(CONTEXT, MAP_LINKS, Toast.LENGTH_LONG).show();
                break;
            case "telegram":
                //Telegram action
                break;
            case "contactAlert":
                sendContactEmergencyMessage();
            default:
                Toast.makeText(CONTEXT,"It's not implemented yet", Toast.LENGTH_LONG).show();
        }
    }

    private void sendSMS(String msg, String number) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, msg, null, null);
            Toast.makeText(CONTEXT,"SMS send successfully: " + msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println(e.toString());
            Toast.makeText(CONTEXT,"SMS send failed", Toast.LENGTH_LONG).show();
        }
    }

    private void volumeDownPressedAction() {
        EMERGENCY_COUNT += 1;
        if (EMERGENCY_COUNT.equals(KEY_COUNT)){
            sendMessageWithLocation("contactAlert");
            restartCount();
        }
    }

    public void sendContactEmergencyMessage() {
        String user = PREFERENCES.getString("UserName","Anonimowy") + " " +
                PREFERENCES.getString("UserSurname", "Anonim");
        String number = PREFERENCES.getString("ContactNumber","0");

        String msg = "Policja próbuje mnie zatrzymać! Moja lokalizacja " +
                PREFERENCES.getString("MapLinks","null") +
                " Jak wkrótcę nie odpowiem powiadom grupę antyrepresyjną (+48 722-196-139)! " +
                user;

        sendSMS(msg, number);
    }
}
