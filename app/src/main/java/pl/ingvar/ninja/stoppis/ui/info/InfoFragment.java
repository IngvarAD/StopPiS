package pl.ingvar.ninja.stoppis.ui.info;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import pl.ingvar.ninja.stoppis.PhoneSample;
import pl.ingvar.ninja.stoppis.R;

public class InfoFragment extends Fragment {

    private final String TAG = "InfoFragment";

    private InfoViewModel infoViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private List<PhoneSample> PHONES = new ArrayList<>();

    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);

        readPhoneData();

        recyclerView = root.findViewById(R.id.PhonesList);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PhonesRecyclerViewAdapter(PHONES);
        recyclerView.setAdapter(mAdapter);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        infoViewModel = ViewModelProviders.of(this).get(InfoViewModel.class);
    }

    private void readPhoneData() {
        //TODO set choosable city
        String city = "warszawa";

        InputStream is = getResources().openRawResource(R.raw.phones);
        BufferedReader bReader = new BufferedReader(
                new InputStreamReader(is, StandardCharsets.UTF_8)
        );
        String line = "";
        try {
            while ((line = bReader.readLine()) != null){
                Log.d(TAG, "Processing line: " + line);
                String[] data = line.split(";");
                PhoneSample sample = new PhoneSample(data);
                if (sample.getCity().equals("default") || sample.getCity().equals(city)) {
                    PHONES.add(sample);
                }
            }
        } catch (IOException e) {
            Log.wtf("InfoFragment", "Error reading on line" + line, e);
            e.printStackTrace();
        }
    }

}