package pl.ingvar.ninja.stoppis;

public class PhoneSample {
    private String name;
    private String number;
    private String city;

    public PhoneSample(String[] data){
        String city_data;
        setName(data[0]);
        setNumber(data[1]);
        if (data.length <= 2) {
            city_data = "default";
        } else {
            city_data = data[2];
        }
        setCity(city_data);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
