package pl.ingvar.ninja.stoppis.ui.police;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pl.ingvar.ninja.stoppis.R;

public class ManualAdapter extends RecyclerView.Adapter<ManualAdapter.ManualViewHolder> {

    private List<String[]> QUESTIONS;

    public static class ManualViewHolder extends RecyclerView.ViewHolder {
        public TextView questionView;
        public TextView answerView;

        public ManualViewHolder(View v) {
            super(v);
            questionView = v.findViewById(R.id.question_placeholder);
            answerView = v.findViewById(R.id.answer_placeholder);
        }
    }

    public ManualAdapter(List<String[]> data) {
        this.QUESTIONS = data;
    }

    @Override
    public ManualAdapter.ManualViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.manual_item, parent, false);

        return new ManualViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ManualViewHolder holder, int position) {
        String[] row = QUESTIONS.get(position);
        holder.questionView.setText(row[0]);
        holder.answerView.setText(row[1]);
    }

    @Override
    public int getItemCount() {
        return QUESTIONS.size();
    }

}
